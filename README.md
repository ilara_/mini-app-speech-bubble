# Mini App Speech Bubble

ILHAM AGUNG RIYADI (4210181023)

![Alt Text](mini app.gif)

This mini app is an application that can display data from the JSON file on the website by entering the url of the website JSON and entering the index from the JSON array on the website, then the process of downloading the input data is carried out, the output will display the data from the json file (name and email) into a bubble speech.

NRP: 4210181023 / index: 23

# Code

- [Main code](https://gitlab.com/ilara_/mini-app-speech-bubble/-/blob/master/project%20folder/Assets/script/DownloadData.cs)
- [SimpleJSON](https://gitlab.com/ilara_/mini-app-speech-bubble/-/blob/master/project%20folder/Assets/script/SimpleJSON.cs)
