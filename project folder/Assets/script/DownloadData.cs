﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;
using TMPro;

public class DownloadData : MonoBehaviour  
{
    public string jsonlink;
    public int index;
    public string opsiReq1;
    public string opsiReq2;
    public string opsiReq3;
    public string dataNama;
    public string dataEmail;
    public string dataID;
    public TextMeshProUGUI bubbleText;
    public JSONArray urut;
    private string data;
    
    
    void Start()
    {
        WWW link = new WWW(jsonlink);
        if (string.IsNullOrEmpty(link.error))
        {
            data = link.text;
        }
        StartCoroutine(RequestData(link));

    }
    private string forNama(string data)
    {
        JSONNode urut = SimpleJSON.JSON.Parse(data);
        JSONNode urutan = urut[index];
        JSONNode urutt = urutan[opsiReq1].ToString();
        return urutt;
    }
    private string forEmail(string data)
    {
        JSONNode urut = SimpleJSON.JSON.Parse(data);
        JSONNode urutan = urut[index];
        JSONNode urutt = urutan[opsiReq2].ToString();
        return urutt;
    }
    private string forID(string data)
    {
        JSONNode urut = SimpleJSON.JSON.Parse(data);
        JSONNode urutan = urut[index];
        JSONNode urutt = urutan[opsiReq3].ToString();
        return urutt; 
    }
    public IEnumerator RequestData(WWW req)
    {
        Debug.Log("loading...");
        yield return req;

        if (index > 99)
        {
            print("...................NO DATA.................");
            bubbleText.text = "no data detected";
        }
        else
        {
            Debug.Log(forNama(req.text));
            Debug.Log(forEmail(req.text));
            Debug.Log(forID(req.text));

            dataNama = forNama(req.text);
            dataEmail = forEmail(req.text);
            dataID = forID(req.text);

            bubbleText.text = "hello! my name is " + (dataNama) + ", my email is " + (dataEmail);
        }

        
    }


}
